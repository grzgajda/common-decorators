# Changelog
## 0.3.0 (28.12.2019)
- **[CatchAll]**
-- new decorator - try to catch all instances of `Error`
- **[Catch]**: 
-- `errName` allow to pass empty string which means _catch all errors_

## 0.2.1 (28.12.2019)
- **[Catch]**: 
-- fixed: reference to `this` works properly

## 0.2.0 (28.12.2019)
- **[Catch]**: 
-- fixed: handling errors when constructor type is different from original

## 0.1.1 (28.12.2019)
- fixed: added files to bundle

## 0.1.0 (28.12.2019)aw™1
- **[Catch]**: 
-- feature: simplified method to transform one error into another
-- feature: added tests for multiple use of decorator

## 0.0.3 (21.12.2019)
- fixed: added .npmignore

## 0.0.2 (21.12.2019)
- fixed: Typescript out directory was invalid
- feature: automatically remove directory `dist` before command `build`

## 0.0.1 (21.12.2019)
- feature: decorator: `Catch`
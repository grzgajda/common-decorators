export class CustomError extends Error {}

export class AnotherError extends Error {}

export class AdvancedError extends Error {
  constructor(message: string, anotherMessage: string, nextMessage: string) {
    const newMessage = [message, anotherMessage, nextMessage].join(' ')
    super(newMessage)
  }
}

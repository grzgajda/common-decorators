import {
  AdvancedErrorHandler,
  CatchAndThrowError,
  CatchCustomError,
  CatchErrorReturnArgument,
  ErrorHandlerWithDependency,
  HandleNonErrorMessage,
  MultipleCatchDecorators,
  NoThrowError,
  SimplifyErrorTransform,
} from './ClassMocks'
import { AnotherError, CustomError } from './CustomError'

describe('Catch decorator', () => {
  describe('catch custom error', () => {
    const testInstance = new CatchCustomError()
    it('should catch an error with synchronous way', () => {
      expect(testInstance.test()).toEqual(2)
    })
    it('should catch an error with async/await way', async () => {
      expect(await testInstance.testAsync()).toEqual(2)
    })
    it('should catch an error with promise way', async () => {
      expect(await testInstance.testPromise()).toEqual(2)
    })
  })

  describe('catch custom error and throw another error', () => {
    const testInstance = new CatchAndThrowError()
    it('should catch an error with synchronous way', () => {
      expect(() => testInstance.test()).toThrow(Error)
    })
    it('should catch an error with async/await way', async done => {
      try {
        await testInstance.testAsync()
      } catch (err) {
        expect(err).toBeInstanceOf(Error)
        done()
      }
    })
    it('should catch an error with promise way', async done => {
      testInstance.testPromise().catch(err => {
        expect(err).toBeInstanceOf(Error)
        done()
      })
    })
  })

  describe('should pass original value when no error is thrown', () => {
    const testInstance = new NoThrowError()
    it('should catch an error with synchronous way', () => {
      expect(testInstance.test()).toEqual(2)
    })
    it('should catch an error with async/await way', async () => {
      expect(await testInstance.testAsync()).toEqual(2)
    })
    it('should catch an error with promise way', async () => {
      expect(await testInstance.testPromise()).toEqual(2)
    })
  })

  describe('should catch an error and return argument', () => {
    const testInstance = new CatchErrorReturnArgument()
    it('should catch an error with synchronous way', () => {
      expect(testInstance.test(2)).toEqual(4)
    })
    it('should catch an error with async/await way', async () => {
      expect(await testInstance.testAsync(2)).toEqual(4)
    })
    it('should catch an error with promise way', async () => {
      expect(await testInstance.testPromise(2)).toEqual(4)
    })
  })

  describe('should allow to use simplified error transform', () => {
    const testInstance = new SimplifyErrorTransform()
    it('should transform custom error to error in synchronous way', () => {
      expect(() => testInstance.test()).toThrow(AnotherError)
    })

    it('should transform custom error to error in async/await way', async done => {
      try {
        await testInstance.testAsync()
      } catch (err) {
        expect(err).toBeInstanceOf(AnotherError)
        done()
      }
    })

    it('should transform custom error to error in promise way', done => {
      testInstance.testPromise().catch(err => {
        expect(err).toBeInstanceOf(AnotherError)
        done()
      })
    })
  })

  describe('should allow to decorate with multiple catch', () => {
    const testInstance = new MultipleCatchDecorators()

    it('should return 2 when all exceptions are caught in synchronous way', () => {
      expect(testInstance.test()).toEqual(2)
    })

    it('should return 2 when all exceptions are caught in async/await way', async () => {
      expect(await testInstance.testAsync()).toEqual(2)
    })

    it('should return 2 when all exceptions are caught in async/await way', done => {
      testInstance.testPromise().then(res => {
        expect(res).toEqual(2)
        done()
      })
    })
  })

  describe('should handle non error message', () => {
    const testInstance = new HandleNonErrorMessage()
    it('should throw an error message only in synchronous way', () => {
      expect(() => testInstance.test()).toThrow('lorem ipsum')
    })

    it('should throw an error message only in async/await way', async done => {
      try {
        await testInstance.testAsync()
      } catch (err) {
        expect(err).toEqual('lorem ipsum')
        done()
      }
    })

    it('should throw an error message only in promise way', done => {
      testInstance.testPromise().catch(err => {
        expect(err).toEqual('lorem ipsum')
        done()
      })
    })
  })

  describe('should catch even advanced errors', () => {
    const testInstance = new AdvancedErrorHandler()

    it('should throw CustomError after transformation in synchronous way', () => {
      expect(() => testInstance.test()).toThrow(CustomError)
    })

    it('should throw CustomError after transformation in async/await way', async done => {
      try {
        await testInstance.testAsync()
      } catch (e) {
        expect(e).toBeInstanceOf(CustomError)
        done()
      }
    })

    it('should throw CustomError after transformation in promise way', done => {
      testInstance.testPromise().catch(err => {
        expect(err).toBeInstanceOf(CustomError)
        done()
      })
    })
  })

  describe('do not remove constructor bindings', () => {
    const testService = { test: (): string => 'lorem ipsum' }
    const testInstance = new ErrorHandlerWithDependency(testService)

    it('should return string in synchronous way', () => {
      expect(testInstance.test()).toEqual('lorem ipsum')
    })
  })
})

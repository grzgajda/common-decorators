import { Catch } from '../../src'
import { AdvancedError, AnotherError, CustomError } from './CustomError'

export class CatchCustomError {
  @Catch(CustomError, (): number => 2)
  test(): void {
    throw new CustomError()
  }

  @Catch(CustomError, (): number => 2)
  async testAsync(): Promise<void> {
    throw new CustomError()
  }

  @Catch(CustomError, (): number => 2)
  testPromise(): Promise<void> {
    return Promise.reject(new CustomError())
  }
}

export class CatchAndThrowError {
  @Catch(CustomError, () => {
    throw new Error()
  })
  test(): void {
    throw new CustomError()
  }

  @Catch(CustomError, () => {
    throw new Error()
  })
  async testAsync(): Promise<void> {
    throw new CustomError()
  }

  @Catch(CustomError, () => {
    throw new Error()
  })
  testPromise(): Promise<void> {
    return Promise.reject(new CustomError())
  }
}

export class NoThrowError {
  @Catch(CustomError, () => {
    throw new Error()
  })
  test(): number {
    return 2
  }

  @Catch(CustomError, () => {
    throw new Error()
  })
  async testAsync(): Promise<number> {
    return 2
  }

  @Catch(CustomError, () => {
    throw new Error()
  })
  testPromise(): Promise<number> {
    return Promise.resolve(2)
  }
}

export class CatchErrorReturnArgument {
  @Catch(CustomError, (_, [n]): number => n * 2)
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  test(n: number): void {
    throw new CustomError()
  }

  @Catch(CustomError, (_, [n]): number => n * 2)
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async testAsync(n: number): Promise<void> {
    throw new CustomError()
  }

  @Catch(CustomError, (_, [n]): number => n * 2)
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  testPromise(n: number): Promise<void> {
    return Promise.reject(new CustomError())
  }
}

export class SimplifyErrorTransform {
  @Catch(CustomError, AnotherError)
  test(): void {
    throw new CustomError()
  }

  @Catch(CustomError, AnotherError)
  async testAsync(): Promise<void> {
    throw new CustomError()
  }

  @Catch(CustomError, AnotherError)
  testPromise(): Promise<void> {
    return Promise.reject(new CustomError())
  }
}

export class MultipleCatchDecorators {
  @Catch(AnotherError, () => 2)
  @Catch(CustomError, AnotherError)
  test(): void {
    throw new CustomError()
  }

  @Catch(AnotherError, () => 2)
  @Catch(CustomError, AnotherError)
  async testAsync(): Promise<void> {
    throw new CustomError()
  }

  @Catch(AnotherError, () => 2)
  @Catch(CustomError, AnotherError)
  testPromise(): Promise<void> {
    return Promise.reject(new CustomError())
  }
}

export class HandleNonErrorMessage {
  @Catch(CustomError, () => {
    throw 'lorem ipsum'
  })
  test(): void {
    throw new CustomError()
  }

  @Catch(CustomError, () => {
    throw 'lorem ipsum'
  })
  async testAsync(): Promise<void> {
    throw new CustomError()
  }

  @Catch(CustomError, () => {
    throw 'lorem ipsum'
  })
  testPromise(): Promise<void> {
    return Promise.reject(new CustomError())
  }
}

export class AdvancedErrorHandler {
  @Catch(AdvancedError, CustomError)
  test(): void {
    throw new AdvancedError('', '', '')
  }

  @Catch(AdvancedError, CustomError)
  async testAsync(): Promise<void> {
    throw new AdvancedError('', '', '')
  }

  @Catch(AdvancedError, CustomError)
  testPromise(): Promise<void> {
    return Promise.reject(new AdvancedError('', '', ''))
  }
}

export class ErrorHandlerWithDependency {
  constructor(private readonly testService: { test(): string }) {}

  @Catch(Error, Error)
  test(): string {
    return this.testService.test()
  }
}

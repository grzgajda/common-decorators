import { AnotherError, CustomError } from './CustomError'
import { CatchAll } from '../../src'

export class BasicCatching {
  @CatchAll(() => 2)
  test(): void {
    throw new CustomError()
  }

  @CatchAll(() => 2)
  async testAsync(): Promise<void> {
    throw new CustomError()
  }

  @CatchAll(() => 2)
  testPromise(): Promise<void> {
    return Promise.reject(new CustomError())
  }
}

export class BasicTransforming {
  @CatchAll(AnotherError)
  test(): void {
    throw new CustomError()
  }

  @CatchAll(AnotherError)
  async testAsync(): Promise<void> {
    throw new CustomError()
  }

  @CatchAll(AnotherError)
  testPromise(): Promise<void> {
    return Promise.reject(new CustomError())
  }
}

export class NonObjectError {
  @CatchAll(() => 2)
  test(): void {
    throw 'lorem ipsum'
  }
}

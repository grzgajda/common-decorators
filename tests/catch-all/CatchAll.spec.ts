import { BasicCatching, BasicTransforming, NonObjectError } from './ClassMocks'
import { AnotherError } from './CustomError'

describe('CatchAll decorator', () => {
  describe('catch CustomError', () => {
    const testInstance = new BasicCatching()
    it('should return 2 in synchronous way', () => {
      expect(testInstance.test()).toEqual(2)
    })

    it('should return 2 in async/await way', async () => {
      expect(await testInstance.testAsync()).toEqual(2)
    })

    it('should return 2 in promise way', done => {
      testInstance.testPromise().then(res => {
        expect(res).toEqual(2)
        done()
      })
    })
  })

  describe('catch CustomError and transform to AnotherError', () => {
    const testInstance = new BasicTransforming()
    it('should throw another error in synchronous way', () => {
      expect(() => testInstance.test()).toThrow(AnotherError)
    })

    it('should throw another error in async/await way', async done => {
      try {
        await testInstance.testAsync()
      } catch (err) {
        expect(err).toBeInstanceOf(AnotherError)
        done()
      }
    })

    it('should throw another error in promise way', done => {
      testInstance.testPromise().catch(err => {
        expect(err).toBeInstanceOf(AnotherError)
        done()
      })
    })
  })

  describe('should catch non-object errors', () => {
    const testInstance = new NonObjectError()
    it('should return 2 in synchronous way', () => {
      expect(testInstance.test()).toEqual(2)
    })
  })
})

/* eslint-disable @typescript-eslint/no-explicit-any */
export type Callback = (...args: any[]) => any
export type CallbackAsync<T = any> = (...args: T[]) => Promise<any>
export type ErrorMessage<T = Error> = { new (...args: any[]): T }

export interface ErrHandler<E extends Error = Error> extends Function {
  (err: E, args: any[]): any
}

function isConstructor(f: any): boolean {
  try {
    new f()
  } catch (err) {
    if (err.message.indexOf('is not a constructor') >= 0) {
      return false
    }
  }
  return true
}

export class HandleDefinition {
  constructor(
    private readonly errName: ErrorMessage | '',
    private readonly errHandler: ErrHandler<Error> | ErrorMessage,
    readonly args: any[],
  ) {}

  handlerError(err: Error): any {
    if (this.errName === '' || err instanceof this.errName) {
      const errHandler: any = this.errHandler
      if (isConstructor(errHandler)) {
        throw new errHandler(err.message)
      }

      return errHandler(err, this.args)
    }

    throw err
  }
}

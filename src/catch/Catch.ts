/* eslint-disable @typescript-eslint/no-explicit-any */
import { ErrHandler, ErrorMessage, HandleDefinition } from './handleDefinition'

export function Catch(
  errName: ErrorMessage | '',
  errHandler: ErrHandler | ErrorMessage,
): MethodDecorator {
  return function(
    target: Record<string, any>,
    propertyName: string,
    propertyDesciptor: PropertyDescriptor,
  ): PropertyDescriptor {
    const method = propertyDesciptor.value
    propertyDesciptor.value = function(...args: any[]): any | Promise<any> {
      const options = new HandleDefinition(errName, errHandler, args)

      try {
        const result = method.apply(this, args)
        if (result instanceof Promise) {
          return result.catch(err => options.handlerError(err))
        }

        return result
      } catch (err) {
        return options.handlerError(err)
      }
    }

    return propertyDesciptor
  }
}

import { Catch } from '../catch/Catch'
import { ErrHandler, ErrorMessage } from '../catch/handleDefinition'

export function CatchAll(
  errHandler: ErrHandler | ErrorMessage,
): MethodDecorator {
  return Catch('', errHandler)
}
